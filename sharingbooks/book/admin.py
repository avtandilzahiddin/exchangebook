from django.contrib import admin

from book.models import Categories, Book, Likes, Comments, ExchangeRequests

admin.site.register(Categories)
admin.site.register(Likes)
admin.site.register(ExchangeRequests)

class CommentsInLine(admin.TabularInline):
    model = Comments
    extra = 0

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    inlines = [CommentsInLine]

# admin.site.register(Book)
#
# admin.site.register(Comments)