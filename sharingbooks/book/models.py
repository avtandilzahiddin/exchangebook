from django.db import models
from django.conf import settings

class Categories(models.Model):
    name = models.CharField('Название категории', max_length=255)
    description = models.TextField('Описание')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('id',)

    def __str__(self):
        return f'{self.name} | {self.description}'

class Book(models.Model):
    owner = models.ForeignKey('user.Profile', models.SET_NULL, 'books', null=True)
    name = models.CharField('Название книги', max_length=255, blank=True, null=True)
    writer = models.CharField('Автор книги', max_length=255, blank=True)
    description = models.TextField('Описание книги')
    image = models.FileField('Фото', upload_to='book_images/', null=True)
    category = models.ForeignKey('book.Categories', models.CASCADE, related_name='category', blank=True, null=True)

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'

    def __str__(self):
        return f'{self.name} | {self.writer}'

class Comments(models.Model):
    text = models.TextField()
    book = models.ForeignKey('book.Book', models.CASCADE, 'book_comments')
    owner = models.ForeignKey('user.Profile', models.CASCADE, 'user_comments')
    created_date = models.DateTimeField('Дата публикации', auto_now_add=True)

    class Meta:
        verbose_name = 'Коментарий'
        verbose_name_plural = 'Коментарии'

class Likes(models.Model):
    book = models.ForeignKey('book.Book', models.CASCADE, 'book_likes')
    user = models.ForeignKey('user.Profile', models.SET_NULL, 'likes_from_profile', null=True)


class ExchangeRequests(models.Model):
    owner = models.ForeignKey('user.Profile', models.SET_NULL, 'requested_user', null=True)
    book = models.ForeignKey('book.Book', models.CASCADE, 'book', null=True)

    class Meta:
        verbose_name = 'Звпрос на обмен'
        verbose_name_plural = 'Запросы на обмен'

    def __str__(self):
        return f'{self.book}'