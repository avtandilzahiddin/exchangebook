from rest_framework import serializers
from django.conf import settings
from book.models import Categories, Book, Comments, Likes, ExchangeRequests
from user.models import Profile
from user.serializers import ProfileSerializer


class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = ('id', 'name', 'description')

class CommentSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)

    class Meta:
        model = Comments
        fields = '__all__'

    def create(self, validated_data):
        user = self.context.get('request').user.profile
        comment = Comments.objects.create(owner=user, **validated_data)
        return comment

    def update(self, instance, validated_data):
        data = validated_data.copy()
        data.pop('book', None)
        for attr, value, in data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

#-----------------------------------------------------------------------------------------------------------------------

class BookCreateSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True, many=False)
    book_comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = ('id', 'name', 'writer', 'description', 'image', 'book_comments', 'owner', 'category')

    def create(self, validated_data):
        owner = self.context.get('request').user.profile
        book = Book.objects.create(owner=owner, **validated_data)
        book.save()
        return book


class BookDetailSerializer(BookCreateSerializer):
    category = CategoriesSerializer()

#-----------------------------------------------------------------------------------------------------------------------
class ExchangeSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)
    book = BookDetailSerializer(read_only=True)
    book_id = serializers.IntegerField()

    class Meta:
        model = ExchangeRequests
        fields = ('id', 'book', 'owner', 'book_id')

    def create(self, validated_data):
        owner = self.context.get('request').user.profile
        book_id = self.validated_data.get('book_id')
        book_self = Book.objects.get(id=book_id)
        exchange = ExchangeRequests.objects.create(book=book_self, owner=owner)
        return exchange
#-----------------------------------------------------------------------------------------------------------------------

class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Likes
        fields = '__all__'