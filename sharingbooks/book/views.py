from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from book.models import Categories, Book, Comments, Likes, ExchangeRequests
from book.permissions import Categories_IsStuffOrReadOnly, Book_IsStuffOrReadOnly, IsCommentOwnerOrReadOnly, \
    IsExchangeOwnerOrReadOnly
from book.serializers import (
    CategoriesSerializer, BookDetailSerializer, CommentSerializer, BookCreateSerializer,
    ExchangeSerializer
)
from rest_framework import filters, status


class CategoryView(ModelViewSet):
    serializer_class = CategoriesSerializer
    queryset = Categories.objects.all()
    lookup_field = 'pk'
    permission_classes = (Categories_IsStuffOrReadOnly,)

#-----------------------------------------------------------------------------------------------------------------------
class BookView(ModelViewSet):
    serializer_class = BookDetailSerializer
    serializer_for_zaha = BookCreateSerializer
    serializer_action_classes = {
        'create': BookCreateSerializer,
    }

    queryset = Book.objects.all()
    lookup_field = 'pk'
    permission_classes = (Book_IsStuffOrReadOnly,)
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'author']

    def get_serializer_class(self):
        if self.action is 'create':
            return self.serializer_for_zaha
        return super(BookView, self).get_serializer_class()

#-----------------------------------------------------------------------------------------------------------------------

class CommentView(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comments.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsCommentOwnerOrReadOnly, )

class BookLikesView(APIView):

    def get(self, request, pk):
        book = Book.objects.get(id=pk)
        likes = Likes.objects.values_list('user__user__username', flat=True).filter(book=book)
        return Response(likes)

class LikesView(APIView):

    def get(self, request, pk):
        user = request.user.profile
        book = Book.objects.get(id=pk)
        if Likes.objects.filter(user=user, book=book).exists():
            Likes.objects.filter(user=user, book=book).delete()
            return Response('Like Deleted', status=status.HTTP_201_CREATED)
        else:
            Likes.objects.create(user=user, book=book)
            return Response('Like Created', status=status.HTTP_200_OK)

#-----------------------------------------------------------------------------------------------------------------------

class ExchangeRequestsView(ModelViewSet):
    serializer_class = ExchangeSerializer
    queryset = ExchangeRequests.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsExchangeOwnerOrReadOnly, )


#-----------------------------------------------------------------------------------------------------------------------
