from django.db import models

class Quote(models.Model):
    text = models.TextField('Текст Цитаты')
    author = models.CharField('Автор', max_length=255)

    class Meta:
        verbose_name = 'Цитата'
        verbose_name_plural = 'Цитаты'
        ordering = ('id', )

    def __str__(self):
        return f'{self.text} © {self.author}'