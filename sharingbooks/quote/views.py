import random
from operator import itemgetter
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from quote.models import Quote
from quote.serializers import QuoteSerializer


class QuoteView(ModelViewSet):
    serializer_class = QuoteSerializer
    queryset = Quote.objects.all()
    lookup_field = 'pk'
