"""sharingbooks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from quote.views import QuoteView
from user.views import ProfileRegisterView, ProfileLoginView, ProfileView
from book.views import CategoryView, BookView, CommentView, LikesView, ExchangeRequestsView, BookLikesView
    #ExchangesView

urlpatterns = [
    path('admin/', admin.site.urls),
    #------------------------------------------------------------------------------------------------------------------
    path('user/register', ProfileRegisterView.as_view()),
    path('user/login', ProfileLoginView.as_view()),
    # ------------------------------------------------------------------------------------------------------------------
    path('user/<int:pk>', ProfileView.as_view({'get': 'retrieve'})),
    path('user/', ProfileView.as_view({'get': 'list'})),
    # ------------------------------------------------------------------------------------------------------------------
    path('quote', QuoteView.as_view({'get': 'list', 'post': 'create'})),
    path('quote/<int:pk>', QuoteView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    # ------------------------------------------------------------------------------------------------------------------
    path('category/', CategoryView.as_view({'get': 'list', 'post': 'create'})),
    path('category/<int:pk>', CategoryView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    # ------------------------------------------------------------------------------------------------------------------
    path('book/', BookView.as_view({'get': 'list', 'post': 'create'})),
    path('book/<int:pk>', BookView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    # ------------------------------------------------------------------------------------------------------------------
    path('comment', CommentView.as_view({'post': 'create'})),
    path('comment/<int:pk>', CommentView.as_view({'put': 'update', 'delete': 'destroy'})),
    #------------------------------------------------------------------------------------------------------------------
    path('like/<int:pk>', LikesView.as_view()),
    path('book/<int:pk>/likes', BookLikesView.as_view()),
    #-----------------------------------
    path('exchange/', ExchangeRequestsView.as_view({'post': 'create', 'get': 'list'})),
    path('exchange/<int:pk>', ExchangeRequestsView.as_view({'get': 'retrieve', 'delete': 'destroy'})),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
