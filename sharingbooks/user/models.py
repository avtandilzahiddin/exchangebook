from django.conf import settings
from django.db import models



class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, models.CASCADE, related_name='profile', null=True)
    first_name = models.CharField('Имя', max_length=255, blank=True, null=True)
    last_name = models.CharField('Фамилие', max_length=255, blank=True, null=True)
    phone = models.CharField(verbose_name='Номер телефона', max_length=255)
    bio = models.TextField('Bio')
    dob = models.DateField('Дата рождния', null=True)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    def __str__(self):
        return f' {self.first_name} {self.last_name}'
