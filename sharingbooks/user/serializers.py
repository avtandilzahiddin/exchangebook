from django.contrib.auth import get_user_model
from rest_auth.models import TokenModel
from rest_framework import serializers

from book.models import Book
from user.models import Profile

# ---------------------------------------------------------------------------------------------------------------------

class ProfileRegisterSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    dob = serializers.DateField()
    phone = serializers.CharField()
    bio = serializers.CharField()

    def create(self, validated_data):
        user = get_user_model().objects.create(username=validated_data.get('username'))
        user.set_password(validated_data.get('password'))
        user.save()
        TokenModel.objects.create(user=user)
        profile = Profile.objects.create(
            user=user,
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            dob=validated_data.get('dob'),
            phone=validated_data.get('phone'),
            bio=validated_data.get('bio')
        )
        return profile


class ProfileLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

class BookSerializerForProfile(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'name', 'writer', 'description', 'image', 'book_comments')

class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True, source='user.username')
    books = BookSerializerForProfile(many=True)

    class Meta:
        model = Profile
        fields = ('id', 'username', 'first_name', 'last_name', 'phone', 'bio', 'dob', 'books')
