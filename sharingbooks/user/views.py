from django.contrib.auth import authenticate
from rest_auth.models import TokenModel
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from django.contrib.auth import get_user_model
from user.models import Profile
from user.serializers import ProfileRegisterSerializer, ProfileLoginSerializer, ProfileSerializer


# -------------------------------------------------------------------------------------------------------------------

class ProfileRegisterView(CreateAPIView):
    serializer_class = ProfileRegisterSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        userid = get_user_model().objects.get(username=serializer.validated_data['username']).id
        token = TokenModel.objects.get(user=userid)
        return Response({'key': token.key}, status=status.HTTP_201_CREATED)


class ProfileLoginView(CreateAPIView):
    serializer_class = ProfileLoginSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.data.get('username')
        password = serializer.data.get('password')
        user = authenticate(request, username=username, password=password)
        userid = get_user_model().objects.get(username=username).id
        if user:
            token = TokenModel.objects.get(user=user)
            return Response({'key': token.key, 'id': userid}, status=status.HTTP_201_CREATED)
        return Response('invalid login', status=status.HTTP_401_UNAUTHORIZED)

class ProfileView(ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()
    lookup_field = 'pk'


